extends TextureProgress

export (float, 0, 1) var min_aplha
export (float, 0, 1) var max_aplha

func _ready():
	if value <= 0:
		_change_alpha(0)
	else:
		_change_alpha(value / max_value)


func _on_value_changed(value):
	if value <= 0:
		_change_alpha(0)
	else:
		_change_alpha(value / max_value)


func _change_alpha(value: float):
	var alpha
	
	if value > .1:
		alpha = range_lerp(1 - value, 0, 1, min_aplha, max_aplha)
	elif value == 0:
		alpha = 0
	else:
		alpha = .8
	
	tint_under = Color(1, 1, 1, alpha)
	tint_progress = Color(1, 1, 1, alpha)
