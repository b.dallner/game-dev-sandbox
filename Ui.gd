extends Control

export (int) var margin_horizontal := 40
export (int) var margin_vertical := 40

func _ready():
	_calc_anchor()


func _calc_anchor():
	var window_size = OS.window_size
	
	anchor_left = margin_horizontal / window_size.x
	anchor_right = 1 - margin_horizontal / window_size.x
	anchor_top = margin_vertical / window_size.y
	anchor_bottom = 1 - margin_vertical / window_size.y

func _on_HSlider_value_changed(value):
	_change_hp_bar(value)
	_change_ammo_bar(value)

func _on_HSlider2_value_changed(value):
	_change_shield_bar(value)
	_change_shield_overlap_bar(value)

func _change_hp_bar(value):
	$CrosshairContainer/hp.value = $CrosshairContainer/hp.max_value * value / 100

func _change_shield_bar(value):
	$CrosshairContainer/shield.value = $CrosshairContainer/shield.max_value * value / 100
	
func _change_shield_overlap_bar(value):
	$CrosshairContainer/shieldOverlap.value = $CrosshairContainer/shieldOverlap.max_value * value / 100


func _change_ammo_bar(value):
	$CrosshairContainer/ammo.value = $CrosshairContainer/ammo.max_value * value / 100

func _input(event):
	if event is InputEventMouseButton:
		if Input.is_action_pressed("shoot"):
			$CrosshairContainer/Crosshair.shoot()
		
		elif Input.is_action_just_released("shoot"):
			$CrosshairContainer/Crosshair.reset()
		
		elif Input.is_action_just_pressed("rand"):
			$CrosshairContainer/Crosshair.color = Color(randf() + .2, randf() + .2, randf() + .2)


