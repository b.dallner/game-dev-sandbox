extends Control

export (NodePath) onready var animation_player = get_node(animation_player) as AnimationPlayer

export (NodePath) onready var center = get_node(center) as Node2D

export (NodePath) onready var pivot_top = get_node(pivot_top) as Node2D
export (NodePath) onready var pivot_right = get_node(pivot_right) as Node2D
export (NodePath) onready var pivot_bottom = get_node(pivot_bottom) as Node2D
export (NodePath) onready var pivot_left = get_node(pivot_left) as Node2D

export (Vector2) var base_offset = Vector2(8,8)

var _shooting: bool = false
var _recoil = .5

var color := Color(1, 1, 1) setget set_color


func _physics_process(delta):
	_update_pivots()


func _update_pivots():
	pivot_top.position.y = lerp(pivot_top.position.y, -base_offset.y, _recoil)
	pivot_right.position.x = lerp(pivot_right.position.x, base_offset.x, _recoil)
	pivot_bottom.position.y = lerp(pivot_bottom.position.y, base_offset.y, _recoil)
	pivot_left.position.x = lerp(pivot_left.position.x, -base_offset.x, _recoil)


func shoot():
	if not _shooting:
		animation_player.play("Shoot")
		_shooting = true


func reset():
	animation_player.play_backwards("Shoot")
	_shooting = false


func set_color(color: Color):
	center.modulate = color
	pivot_top.modulate = color
	pivot_right.modulate = color
	pivot_bottom.modulate = color
	pivot_left.modulate = color
